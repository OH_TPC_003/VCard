/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export namespace contact {
    /**
     * Provides methods for contact information
     */
    export class Contact {
        /**
         * Indicates the contact ID.
         */
        static readonly INVALID_CONTACT_ID: -1

        /**
         * Indicates the contact ID.
         */
        readonly id: number | null =  -1

        /**
         * Indicates the query key that identifies the contact.
         */
        readonly key: string | null = null

        /**
         * Indicates the contact attributes.
         */
        contactAttributes: ContactAttributes | null = null

        /**
         * Indicates an email address of the contact.
         */
        emails: Email[] | null = null

        /**
         * Indicates an event (special date) of the contact.
         */
        events: Event[] | null = null

        /**
         * Indicates a group of the contact.
         */
        groups: Group[] | null = null

        /**
         * Indicates an IM address of the contact.
         */
        imAddresses: ImAddress[] | null = null

        /**
         * Indicates a phone number of the contact.
         */
        phoneNumbers: PhoneNumber[] | null  = null

        /**
         * Indicates the contact portrait.
         */
        portrait: Portrait|null = null

        /**
         * Indicates a postal address of the contact.
         */
        postalAddresses: PostalAddress[] | null = null

        /**
         * Indicates a relation of the contact.
         */
        relations: Relation[] | null = null

        /**
         * Indicates a Session Initiation Protocol (SIP) address of the contact.
         */
        sipAddresses: SipAddress[] | null = null

        /**
         * Indicates a website of the contact.
         */
        websites: Website[] | null = null

        /**
         * Indicates the contact name.
         */
        name: Name | null = null

        /**
         * Indicates the contact nickname.
         */
        nickName: NickName | null = null

        /**
         * Indicates the contact note.
         */
        note: Note | null = null

        /**
         * Indicates organization information about the contact.
         */
        organization: Organization | null = null
    }

    /**
     * Provides methods for contact attributes information
     */
    export class ContactAttributes {
        /**
         * Indicates the contact attributes.
         */
        attributes: Attribute[] | null = null
    }

    /**
     * Provides methods for attribute information
     */
    enum Attribute {
        /**
         * Indicates the contact event.
         */
        ATTR_CONTACT_EVENT,

        /**
         * Indicates the email address.
         */
        ATTR_EMAIL,

        /**
         * Indicates the contact group.
         */
        ATTR_GROUP_MEMBERSHIP,

        /**
         * Indicates the instant messaging (IM) address.
         */
        ATTR_IM,

        /**
         * Indicates the name.
         */
        ATTR_NAME,

        /**
         * Indicates the nickname.
         */
        ATTR_NICKNAME,

        /**
         * Indicates the note.
         */
        ATTR_NOTE,

        /**
         * Indicates the organization.
         */
        ATTR_ORGANIZATION,

        /**
         * Indicates the phone number.
         */
        ATTR_PHONE,

        /**
         * Indicates the portrait.
         */
        ATTR_PORTRAIT,

        /**
         * Indicates the postal address.
         */
        ATTR_POSTAL_ADDRESS,

        /**
         * Indicates the relation.
         */
        ATTR_RELATION,

        /**
         * Indicates the Session Initiation Protocol (SIP) address.
         */
        ATTR_SIP_ADDRESS,

        /**
         * Indicates the website.
         */
        ATTR_WEBSITE
    }

    /**
     * Provides methods for email information
     */
    export class Email {
        /**
         * Indicates a custom label.
         */
        static readonly CUSTOM_LABEL= 0

        /**
         * Indicates a home email.
         */
        static readonly EMAIL_HOME= 1

        /**
         * Indicates a work email.
         */
        static readonly EMAIL_WORK= 2

        /**
         * Indicates an email of the OTHER type.
         */
        static readonly EMAIL_OTHER= 3

        /**
         * Indicates an invalid label ID.
         */
        static readonly INVALID_LABEL_ID= -1

        /**
         * Indicates the email address.
         */
        email: string = ''

        /**
         * Indicates the label name of an attribute.
         */
        labelName : string | null = null

        /**
         * Indicates the displayed email name.
         */
        displayName:string | null = null

        /**
         * Indicates the label id.
         */
        labelId:number =0;
    }

    /**
     * Provides methods for event information
     */
    export class Event {
        /**
         * Indicates a custom label.
         */
        static readonly CUSTOM_LABEL= 0

        /**
         * Indicates an anniversary event.
         */
        static readonly EVENT_ANNIVERSARY= 1

        /**
         * Indicates an event of the OTHER type.
         */
        static readonly EVENT_OTHER= 2

        /**
         * Indicates an birthday event.
         */
        static readonly EVENT_BIRTHDAY= 3

        /**
         * Indicates an invalid label ID.
         */
        static readonly INVALID_LABEL_ID= -1

        /**
         * Indicates the event date.
         */
        eventDate: string | ESObject = null

        /**
         * Indicates the label name of an attribute.
         */
        labelName: string | null = null

        /**
         * Indicates the label id.
         */
        labelId: number | ESObject = null
    }

    /**
     * Provides methods for group information
     */
    export class Group {
        /**
         * Indicates the contact group ID.
         */
        groupId: number | null = null

        /**
         * Indicates the contact group title.
         */
        title: string | null = null
    }

    /**
     * Provides methods for holder information
     */
    export class Holder {
        /**
         * Indicates the bundle name of a contact holder.
         */
        bundleName: string | null = null

        /**
         * Indicates the displayed name of a contact holder.
         */
        displayName: string | null = null

        /**
         * Indicates the holder ID.
         */
        holderId: number | null = null
    }

    /**
     * Provides methods for ImAddress information
     */
    export class ImAddress {
        /**
         * Indicates a custom label.
         */
        static readonly CUSTOM_LABEL= -1

        /**
         * Indicates an AIM instant message.
         */
        static readonly IM_AIM= 0

        /**
         * Indicates a Windows Live instant message.
         */
        static readonly IM_MSN= 1

        /**
         * Indicates a Yahoo instant message.
         */
        static readonly IM_YAHOO= 2

        /**
         * Indicates a Skype instant message.
         */
        static readonly IM_SKYPE= 3

        /**
         * Indicates a QQ instant message.
         */
        static readonly IM_QQ= 4

        /**
         * Indicates an ICQ instant message.
         */
        static readonly IM_ICQ= 6

        /**
         * Indicates a Jabber instant message.
         */
        static readonly IM_JABBER= 7

        /**
         * Indicates an invalid label ID.
         */
        static readonly INVALID_LABEL_ID= -2

        /**
         * Indicates the IM address.
         */
        imAddress: string | ESObject = null

        /**
         * Indicates the label name of an attribute.
         */
        labelName: string | null = null

        /**
         * Indicates the label id.
         */
        labelId: number | ESObject = null
    }

    /**
     * Provides methods for name information
     */
    export class Name {
        /**
         * Indicates the family name of the contact.
         */
        familyName: string | null = null

        /**
         * Indicates the phonetic family name of the contact.
         */
        familyNamePhonetic: string | null = null

        /**
         * Indicates the full name of the contact.
         */
        fullName: string | null = null

        /**
         * Indicates the given name of the contact.
         */
        givenName: string | null = null

        /**
         * Indicates the phonetic given name of the contact.
         */
        givenNamePhonetic: string | ESObject = null

        /**
         * Indicates the middle name of the contact.
         */
        middleName: string | null = null

        /**
         * Indicates the phonetic middle name of the contact.
         */
        middleNamePhonetic: string | null = null

        /**
         * Indicates the prefix of the contact name.
         */
        namePrefix: string | null = null

        /**
         * Indicates the suffix of this contact name.
         */
        nameSuffix: string | null = null
    }

    /**
     * Provides methods for nick name information
     */
    export class NickName {
        /**
         * Indicates the nickname of the contact.
         */
        nickName: string | ESObject = null
    }

    /**
     * Provides methods for note information
     */
    export class Note {
        /**
         * Indicates the note content.
         */
        noteContent: string | ESObject = null
    }

    /**
     * Provides methods for organization information
     */
    export class Organization {
        /**
         * Indicates the name of the organization to which the contact belongs.
         */
        name: string | ESObject = null

        /**
         * Indicates the title of the contact.
         */
        title: string | ESObject = null
    }

    /**
     * Provides methods for phone number information
     */
    export class PhoneNumber {
        /**
         * Indicates a custom label.
         */
        static readonly CUSTOM_LABEL= 0

        /**
         * Indicates a home number.
         */
        static readonly NUM_HOME= 1

        /**
         * Indicates a mobile phone number.
         */
        static readonly NUM_MOBILE= 2

        /**
         * Indicates a work number.
         */
        static readonly NUM_WORK= 3

        /**
         * Indicates a work fax number.
         */
        static readonly NUM_FAX_WORK= 4

        /**
         * Indicates a home fax number.
         */
        static readonly NUM_FAX_HOME= 5

        /**
         * Indicates a pager number.
         */
        static readonly NUM_PAGER= 6

        /**
         * Indicates a number of the OTHER type.
         */
        static readonly NUM_OTHER= 7

        /**
         * Indicates a callback number.
         */
        static readonly NUM_CALLBACK= 8

        /**
         * Indicates a car number.
         */
        static readonly NUM_CAR= 9

        /**
         * Indicates a company director number.
         */
        static readonly NUM_COMPANY_MAIN= 10

        /**
         * Indicates an Integrated Services Digital Network (ISDN) number.
         */
        static readonly NUM_ISDN= 11

        /**
         * Indicates a main number.
         */
        static readonly NUM_MAIN= 12

        /**
         * Indicates a number of the OTHER_FAX type.
         */
        static readonly NUM_OTHER_FAX= 13

        /**
         * Indicates a radio number.
         */
        static readonly NUM_RADIO= 14

        /**
         * Indicates a telex number.
         */
        static readonly NUM_TELEX= 15

        /**
         * Indicates a teletypewriter (TTY) or test-driven development (TDD) number.
         */
        static readonly NUM_TTY_TDD= 16

        /**
         * Indicates a work mobile phone number.
         */
        static readonly NUM_WORK_MOBILE= 17

        /**
         * Indicates a work pager number.
         */
        static readonly NUM_WORK_PAGER= 18

        /**
         * Indicates an assistant number.
         */
        static readonly NUM_ASSISTANT= 19

        /**
         * Indicates an MMS number.
         */
        static readonly NUM_MMS= 20

        /**
         * Indicates an invalid label ID.
         */
        static readonly INVALID_LABEL_ID= -1

        /**
         * Indicates the label name of an attribute.
         */
        labelName: string | null = null

        /**
         * Indicates the phone number of the contact.
         */
        phoneNumber: string | ESObject = null

        /**
         * Indicates the label id.
         */
        labelId: number | ESObject = null
    }

    /**
     * Provides methods for portrait information
     */
    export class Portrait {
        /**
         * Indicates the uri of the contact portrait.
         */
        uri: string | null = null
    }

    /**
     * Provides methods for postal address information
     */
    export class PostalAddress {
        /**
         * Indicates a custom label.
         */
        static readonly CUSTOM_LABEL= 0

        /**
         * Indicates a home address.
         */
        static readonly ADDR_HOME= 1

        /**
         * Indicates a work address.
         */
        static readonly ADDR_WORK= 2

        /**
         * Indicates an address of the OTHER type.
         */
        static readonly ADDR_OTHER= 3

        /**
         * Indicates an invalid label ID.
         */
        static readonly INVALID_LABEL_ID= -1

        /**
         * Indicates the city where this contact is located.
         */
        city: string | null = null

        /**
         * Indicates the country/region where this contact is located.
         */
        country: string | null = null

        /**
         * Indicates the label name of an attribute.
         */
        labelName: string | null = null

        /**
         * Indicates the neighborhood where this contact is located.
         */
        neighborhood: string | null = null

        /**
         * Indicates the post box of this contact.
         */
        pobox: string | null = null

        /**
         * Indicates the postal address of this contact.
         */
        postalAddress: string | null = null

        /**
         * Indicates the postal code of this contact.
         */
        postcode: string | null = null

        /**
         * Indicates the area where this contact is located.
         */
        region: string | ESObject = null

        /**
         * Indicates the street where this contact is located.
         */
        street: string | null = null

        /**
         * Indicates the label id.
         */
        labelId: number | ESObject = null
    }

    /**
     * Provides methods for relation information
     */
    export class Relation {
        /**
         * Indicates a custom label.
         */
        static readonly CUSTOM_LABEL= 0

        /**
         * Indicates an assistant.
         */
        static readonly RELATION_ASSISTANT= 1

        /**
         * Indicates a brother.
         */
        static readonly RELATION_BROTHER= 2

        /**
         * Indicates a child.
         */
        static readonly RELATION_CHILD= 3

        /**
         * Indicates a domestic partner.
         */
        static readonly RELATION_DOMESTIC_PARTNER= 4

        /**
         * Indicates a father.
         */
        static readonly RELATION_FATHER= 5

        /**
         * Indicates a friend.
         */
        static readonly RELATION_FRIEND= 6

        /**
         * Indicates a manager.
         */
        static readonly RELATION_MANAGER= 7

        /**
         * Indicates a mother.
         */
        static readonly RELATION_MOTHER= 8

        /**
         * Indicates a parent.
         */
        static readonly RELATION_PARENT= 9

        /**
         * Indicates a partner.
         */
        static readonly RELATION_PARTNER= 10

        /**
         * Indicates a referrer.
         */
        static readonly RELATION_REFERRED_BY= 11

        /**
         * Indicates a relative.
         */
        static readonly RELATION_RELATIVE= 12

        /**
         * Indicates a sister.
         */
        static readonly RELATION_SISTER= 13

        /**
         * Indicates a spouse.
         */
        static readonly RELATION_SPOUSE= 14

        /**
         * Indicates an invalid label ID.
         */
        static readonly INVALID_LABEL_ID: -1

        /**
         * Indicates the label name of an attribute.
         */
        labelName: string | null = null

        /**
         * Indicates the relation name.
         */
        relationName: string | ESObject = null

        /**
         * Indicates the label id.
         */
        labelId: number | ESObject = null
    }

    /**
     * Provides methods for sip address information
     */
    export class SipAddress {
        /**
         * Indicates a custom label.
         */
        static readonly CUSTOM_LABEL= 0

        /**
         * Indicates a home SIP address.
         */
        static readonly SIP_HOME= 1

        /**
         * Indicates a work SIP address.
         */
        static readonly SIP_WORK= 2

        /**
         * Indicates an SIP address of the OTHER type.
         */
        static readonly SIP_OTHER= 3

        /**
         * Indicates an invalid label ID.
         */
        static readonly INVALID_LABEL_ID= -1

        /**
         * Indicates the label name of an attribute.
         */
        labelName: string | null = null

        /**
         * Indicates the SIP address.
         */
        sipAddress: string | null = null

        /**
         * Indicates the label id.
         */
        labelId: number | null = null
    }

    /**
     * Provides methods for website information
     */
    export class Website {
        /**
         * Indicates the website.
         */
        website: string | ESObject = null
    }
}
