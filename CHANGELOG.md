## 3.0.3
在DevEco Studio: NEXT Beta1-5.0.3.806, SDK: API12 Release (5.0.0.66)验证通过

## 3.0.2
VCardBuilder.appendAndroidSpecificProperty接口变更为VCardBuilder.appendSpecificProperty
VCardConfig.usesAndroidSpecificProperty接口变更为VCardConfig.usesSpecificProperty

## 3.0.1
1.DevEco Studio 版本： 4.1 Canary(4.1.3.317),OpenHarmony SDK:API11 (4.1.0.36)
2.ArkTs 语法适配

## 3.0.0
1.适配DevEco Studio:3.1Release(3.1.3.400), SDK:API9 Release(3.2.11.5)
2.包管理工具由npm切换成ohpm

### 2.2.1

适配DevEco Studio 3.1 Beta1版本

### 2.2.0

1.适配API9 Stage模型。

### 2.1.2

1.移植到hvigorfile编译系统。

### 1.0.0

1.将源语言移植到ts语言,并且进行ArkTS本地api适配。

2.增加了平台化的组件（stringbuilder）,方便后期维护。

3.增加了测试VCard资源。

