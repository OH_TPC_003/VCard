# VCard

## 简介
> VCard是电子名片的文件格式标准。它一般附加在电子邮件之后，但也可以用于其它场合（如在网际网路上相互交换）。
> VCard可包含的信息有：姓名、地址资讯、电话号码、URL，logo，相片等。本库支持VCard标准2.0和3.0。
![img1.png](img1.png)

## 下载安装
```shell
ohpm install @ohos/vcard
```
OpenHarmony ohpm 环境配置等更多内容，请参考 [如何安装OpenHarmony ohpm包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.md)

## 使用说明

### 生成VCard
```
import {contact,VCardBuilder,VCardConfig} from "@ohos/vcard";

  let myCard= new  contact.Contact();
  let nickName = new contact.NickName();
  nickName.nickName ="昵称";
  myCard.nickName = nickName;
  let builder = new VCardBuilder(VCardConfig.VCARD_TYPE_V21_GENERIC,"UTF-8");
  builder.appendNickNames(myCard);
  let result = builder.toString();

```

### 解析VCard文本
```
import fileio from "@ohos.fileio";
import {VCardParserImpI_V20,VCardEntry,VCardInterpreter,VCardProperty} from "@ohos/vcard";


let vCardEntry = new VCardEntry()
class MyVCardInterpreter implements VCardInterpreter{
    onVCardStarted(): void {
    }
    onVCardEnded(): void {
    }
    onEntryStarted(): void {
    }
    onEntryEnded(): void {
    }
    onPropertyCreated(property: VCardProperty): void {
        vCardEntry.addProperty(property)
    }
}
let myParser = new VCardParserImpl_V20()
myParser.addInterpreter(new MyVCardInterpreter() )
let fileDescriptor = fileio.openSync("./pathTo/00002.vcf", 0o100|0o2,0o666)
myParser.parse(fileDescriptor)
vCardEntry.consolidateFields()
let displayName = vCardEntry.getDisplayName()
let emails= vCardEntry.getEmailList()
```
## 接口说明
`let builder=new VCardBuilder(VCardConfig.VCARD_TYPE_V21_GENERIC,"UTF-8");`
1. 昵称
   `builder.appendNickNames()`
2. 名字
   `builder.appendNameProperties()`
3. 电话
   `builder.appendPhones()`
4. 电子邮件
   `builder.appendEmails()`
5. 地址
   `builder.appendPostals()`
6. 社交工具
   `builder.appendIms()`
7. 网站地址
   `builder.appendWebsites()`
8. 组织
   `builder.appendOrganizations()`
9. 照片地址
   `builder.appendPhotos()`
10. 备注
    `builder.appendNotes()`
11. 群组
    `builder.appendGroupMemberShip()`
12. 事件
    `builder.appendEvents()`
13. 关系
    `builder.appendRelation()`

## 约束与限制
在下述版本验证通过：

- DevEco Studio 版本： 4.1 Canary(4.1.3.317),OpenHarmony SDK:API11 (4.1.0.36)。
- DevEco Studio: NEXT Beta1-5.0.3.806, SDK: API12 Release (5.0.0.66)。

## 目录结构
````
|---- VCard  
|     |---- entry  # 示例代码文件夹
|     |---- library  # vcard库文件夹
|           |---- index.ets  # 对外接口
|     |---- README.MD  # 安装使用方法                    
````

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-tpc/VCard/issues) 给组件，当然，也非常欢迎发 [PR](https://gitee.com/openharmony-tpc/VCard/pulls) 共建。

## 开源协议
本项目基于 [Apache License 2.0](https://gitee.com/openharmony-tpc/VCard/blob/master/LICENSE) 协议，请自由地享受和参与开源。